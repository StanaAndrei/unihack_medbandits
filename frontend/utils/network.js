import axios from "axios";
export const baseHttpURL = `https://5e93-84-243-76-48.ngrok-free.app`;

export const axiosInstanceToApi = axios.create({
  baseURL: baseHttpURL,
  timeout: 30000e3,
});

const getJWT = () => {
  return localStorage.getItem("token");
};
