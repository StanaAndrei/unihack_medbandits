import React from "react";

import Footer from "../../components/Footer";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import { styled } from "@mui/material/styles";
import Banner from "../assets/banner.png";
import { useNavigate } from "react-router-dom";
import LogoRound from "../assets/logoRound.png";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import TypewriterText from "../../components/TypeWriter";
// Stiluri suplimentare
const HoverEffectBox = styled(Box)({
  "&:hover": {
    transform: "scale(1.05)",
    transition: "transform 0.3s ease-in-out",
  },
});

const CustomCard = styled(Card)({
  marginBottom: "20px",
  boxShadow: "0 6px 12px rgba(0, 0, 0, 0.1)",
  transition: "box-shadow 0.3s ease-in-out",
  "&:hover": {
    boxShadow: "0 12px 24px rgba(0, 0, 0, 0.2)",
  },
});

const CustomCardContent = styled(CardContent)({
  backgroundColor: "rgba(255, 255, 255, 0.9)",
  borderRadius: "5px",
});

// // Stil pentru spațierea între butoane
// const SpacingBox = styled(Box)({
//   marginLeft: "8px",
// });

// const LogoContainer = styled(Box)({
//   display: "flex",
//   alignItems: "center",
// });

// const LogoImage = styled("img")({
//   width: "40px",
//   marginRight: "8px",
// });

const CustomButton = styled(Button)({
  "&:hover": {
    backgroundColor: "white",
    color: "black",
    borderColor: "#FF0000",
    animation: "pulse 1s infinite",
    transition: "background-color 1s ease, color 1s ease",
  },
  "@keyframes pulse": {
    "0%": {
      transform: "scale(1)",
    },
    "50%": {
      transform: "scale(1.05)",
    },
    "100%": {
      transform: "scale(1)",
    },
  },
});
const StyledContainer = styled(Container)({
  marginTop: "20px",
  padding: "20px",
  backgroundColor: "rgba(255, 255, 255, 0.8)",
  borderRadius: "10px",
  boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
});

const SectionHeader = styled(Typography)({
  fontWeight: "bold",
  marginTop: "20px",
  marginBottom: "10px",
  color: "#333",
});
const parallaxStyle = {
  backgroundImage: `url(${Banner})`,
  minHeight: "100vh",
  backgroundAttachment: "fixed",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  position: "relative",
};

const SectionText = styled(Typography)({
  marginBottom: "10px",
  color: "#555",
  lineHeight: "1.6",
});
// Styles for the buttons
const redButtonStyle = {
  backgroundColor: "#FF0000", // Red background
  color: "white", // White text
  fontWeight: "bold",
  border: "none", // No border for a cleaner look
  // margin: "5px", // Margin for spacing around buttons
  padding: "10px 20px", // Padding for button size
  borderRadius: "5px", // Slightly rounded corners
  cursor: "pointer", // Cursor to indicate button
  outline: "none", // Remove outline on focus for aesthetics
  width: "200px", // Fixed width for consistency
  margin: "0 35px", // Increased margin for spacing between buttons
  marginTop: "50px",
};

// Styles for the container holding the buttons
const buttonContainerStyle = {
  display: "flex", // Display buttons in a row
  justifyContent: "center", // Center buttons horizontally
  alignItems: "center", // Center buttons vertically
  margin: "10px 0", // Margin around the whole container
};
const RedLine = {
  height: "10px", // Thickness of the line
  width: "250px", // Width of the line
  marginLeft: "auto", // Optional: Margin around the line
  marginRight: "auto",
  marginTop: "25px",
  borderRadius: "10px",
  backgroundColor: "#FF0000",
};

function PresPage(props) {
  const navigate = useNavigate();
  return (
    <>
      <div>
        <Box>
          <div style={parallaxStyle}>{}</div>
        </Box>
        <div style={RedLine}></div>
        <div style={buttonContainerStyle}>
          <CustomButton
            style={redButtonStyle}
            onClick={() => navigate("/login")}
          >
            Log In
          </CustomButton>
          <CustomButton
            style={redButtonStyle}
            onClick={() => navigate("/register")}
          >
            Register
          </CustomButton>
        </div>
        <StyledContainer>
          <TypewriterText />
          <HoverEffectBox>
            <CustomCard>
              <CustomCardContent>
                <SectionHeader variant="h5">
                  About the Application
                </SectionHeader>
                <SectionText>
                  Our application represents a revolutionary innovation in the
                  healthcare field, providing an intuitive guidance portal for
                  patients and medical clinics. It is intended for those seeking
                  medical guidance in a simple and efficient manner, eliminating
                  the uncertainty and misinformation obtained from online
                  forums.
                </SectionText>
              </CustomCardContent>
            </CustomCard>
          </HoverEffectBox>
          <HoverEffectBox>
            <CustomCard>
              <CustomCardContent>
                <SectionHeader variant="h6">For Medical Clinics</SectionHeader>
                <SectionText>
                  The application functions as a tool for expanding the patient
                  network. The clinic receives the patient's report directly
                  through the application, with the option to accept or reject
                  the consultation request. This system not only simplifies the
                  process of managing new patients but also increases the
                  visibility of clinics among the application's users.
                </SectionText>
              </CustomCardContent>
            </CustomCard>
          </HoverEffectBox>

          <HoverEffectBox>
            <CustomCard>
              <CustomCardContent>
                <SectionHeader variant="h6">For Patients</SectionHeader>
                <SectionText>
                  The application is a personalized guide for those who are
                  dealing with health issues and are unsure about which medical
                  specialty to turn to. The patient uploads analyses, photos, or
                  describes symptoms, and the application's artificial
                  intelligence evaluates this information, suggesting possible
                  conditions and recommending suitable medical clinics. This
                  significantly reduces confusion and the time wasted in
                  searching for unreliable information on the internet, offering
                  an efficient and accessible solution.
                </SectionText>
              </CustomCardContent>
            </CustomCard>
          </HoverEffectBox>
          <HoverEffectBox>
            <CustomCard>
              <CustomCardContent>
                <SectionHeader variant="h6">Implementation Mode</SectionHeader>
                <SectionText>
                  The user uploads PDF documents with analyses, real-time
                  photos, or describes symptoms. The AI analyzes this data,
                  generating a personalized report with the probability of
                  certain conditions and suggests relevant medical clinics. For
                  instance, if there is a 45% chance of suffering from a
                  particular disease, the application will recommend visiting a
                  specialized clinic.
                </SectionText>
              </CustomCardContent>
            </CustomCard>
          </HoverEffectBox>
        </StyledContainer>

        <Footer />
      </div>
    </>
  );
}

export default PresPage;
