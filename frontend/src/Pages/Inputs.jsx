import React, { useState, useEffect } from "react";
import { axiosInstanceToApi } from "../../utils/network";
import "./Inputs.css"; // Assuming styles are moved to a separate CSS file
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import { useNavigate } from "react-router-dom";
import GoogleMapComponent from "../../components/map";
import Orders from "../../components/Orders";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { StaticTimePicker } from "@mui/x-date-pickers/StaticTimePicker";
import Footer from "../../components/Footer";
import "../App.css";

function Inputs() {
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [symptoms, setSymptoms] = useState("");
  const [area, setArea] = useState(null);
  const [user, setUser] = useState(null);
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const [clinics, setClinics] = useState(null);
  const [date, setDate] = useState(null);
  const [hr, setHr] = useState(null);
  const navigate = useNavigate();

  const handleFileChange = (event) => {
    processFiles(event.target.files);
  };

  const handleSymptomsChange = (event) => {
    setSymptoms(event.target.value);
  };

  const handleDrop = (event) => {
    event.preventDefault();
    processFiles(event.dataTransfer.files);
    event.dataTransfer.clearData();
  };

  const processFiles = (files) => {
    const pdfFiles = Array.from(files).filter(
      (file) => file.type === "application/pdf"
    );
    setSelectedFiles(pdfFiles);
  };

  const toggleDrawer = (open) => {
    console.log("Toggle Drawer Called:", open);
    setDrawerOpen(open);
    console.log("Drawer State:", isDrawerOpen);
  };

  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const [dis, setDis] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append("file", selectedFiles[0]);

    try {
      const response = await axiosInstanceToApi.post("/user/upload", formData);
      console.log(response.data.disease);
      setDis(response.data.disease);
      setArea(response.data.res);
      console.log("----------------------------------");
      const res2 = await axiosInstanceToApi.get(
        `/clinic/get-by-area/${response.data.res}`
      );

      setClinics(res2.data);
      console.log(clinics);
    } catch (err) {
      console.error(err);
    }
  };

  const handleSymptomsByChatGpt = () => {
    axiosInstanceToApi
      .post("/user/upload-text", {
        description: symptoms,
      })
      .then((res) => {
        setSymptoms("");
        setArea(res.data.res);
        axiosInstanceToApi
          .get(`/clinic/get-by-area/${res.data.res}`)
          .then((_res) => {
            console.log(_res.data);
            setClinics(_res.data);
          });
      })
      .catch((err) => {
        alert("Eroare" + err);
      });
  };

  const setHHr = (hr) => {
    console.log(hr.$H);
    setHr(hr);
  };

  console.log(date);

  const handleCreate = async (clinicId) => {
    try {
      const response = await axiosInstanceToApi.post("/appointment/", {
        startDate: date,
        clinicId,
        startHour: hr?.$H,
        userId: user.id,
        name: user.firstName,
      });
      if (response.status === 200) {
        console.log("aa");
        navigate("/home");
      }
    } catch (err) {
      alert("Error");
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (localStorage.getItem("isClinic") === "true") {
          const response = await axiosInstanceToApi.get(
            `/clinic/${localStorage.getItem("token")}`
          );
          console.log(response.data);
          setUser(response.data);
          console.log(user?.isClinic);
        } else {
          const response = await axiosInstanceToApi.get(
            `/user/${localStorage.getItem("token")}`
          );
          console.log(response.data);
          setUser(response.data);
        }
      } catch (err) {
        alert("Eroare" + err);
      }
    };
    fetchData();
  }, []);

  const menuItems = [
    { text: "Item 1", onClick: () => console.log("Item 1 clicked") },
    { text: "Item 2", onClick: () => console.log("Item 2 clicked") },
    // Add more items as needed
  ];

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <div className="container">
        {/* AppBar */}
        <AppBar position="static" style={{ backgroundColor: "#FF0000" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={() => toggleDrawer(!isDrawerOpen)}
              style={{ border: "none", outline: "none" }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" style={{ color: "white" }}>
              MedMe
            </Typography>
          </Toolbar>
        </AppBar>

        {/* Drawer */}
        <Drawer
          anchor="left"
          open={isDrawerOpen}
          onClose={() => toggleDrawer(false)}
        >
          <List>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Dashboard"
                onClick={() => navigate("/home")}
              />
            </ListItem>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Analyse"
                onClick={() => navigate("/inputs")}
              />
            </ListItem>
          </List>
          <List>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Logout"
                onClick={() => {
                  localStorage.removeItem("token");
                  localStorage.removeItem("isClinic");
                  navigate("/");
                }}
              />
            </ListItem>
          </List>
        </Drawer>
        <div className="containerG">
          <div className="title">
            <h1 style={{ color: "black" }}>Analyze Your Blood</h1>
          </div>
          <form onSubmit={handleSubmit} className="form">
            <div
              onDrop={handleDrop}
              onDragOver={handleDragOver}
              className="drop-area"
            >
              Drag and drop a PDF file here
            </div>
            <div style={{ display: "flex", width: "400px" }}>
              <input
                type="file"
                accept="application/pdf"
                multiple
                onChange={handleFileChange}
                id="fileInput"
                className="file-input"
              />
              <label htmlFor="fileInput" className="file-label">
                Choose a file
              </label>
              {selectedFiles.length > 0 && (
                <div className="file-list">
                  {selectedFiles.map((file, index) => (
                    <div key={index} className="file-name">
                      {file.name}
                    </div>
                  ))}
                </div>
              )}
              <button
                type="submit"
                style={{ marginLeft: "20px" }}
                onClick={handleSubmit}
                className="submit-btn"
              >
                Show Results
              </button>
            </div>
          </form>
          <div className="title">
            <h1 style={{ color: "black", marginTop: "10%" }}>
              Analyze Your Symptoms
            </h1>
          </div>
          <form onSubmit={handleSubmit} className="form">
            <textarea
              placeholder="Describe your symptoms"
              value={symptoms}
              onChange={handleSymptomsChange}
              className="textarea"
            />
            <button
              type="submit"
              className="submit-btn"
              onClick={handleSymptomsByChatGpt}
            >
              Show Results
            </button>
            {area && (
              <div>
                {dis !== "" && (
                  <p style={{ color: "black" }}>
                    You have a big chance to have:{" "}
                    <span style={{ fontWeight: "bold" }}>{dis}</span>
                  </p>
                )}

                <p style={{ color: "black" }}>
                  We recommend visiting clinics from the following area:{" "}
                  <span style={{ fontWeight: "bold" }}>{area}</span>
                </p>
                {/* {clinics && (
              <div>
                <p style={{ color: "black" }}>
                  There are some clinics already registered for your problem .
                </p>
                {
              </div>
            )} */}
              </div>
            )}
          </form>
          {clinics?.map((item, index) => {
            console.log(item);
            return (
              <div
                style={{
                  width: "700px",
                  border: "2px #dddddd solid",
                  borderRadius: "8px",
                  padding: "50px",
                  marginTop: "30px",
                }}
                key={index}
              >
                <p style={{ color: "black" }}>
                  {" "}
                  Clinic:{" "}
                  <span
                    style={{
                      fontWeight: "bold",
                      color: "#FF0000",
                    }}
                  >
                    {item.name}
                  </span>
                </p>
                <p style={{ color: "black" }}> Area: {item.area}</p>
                <p style={{ color: "black" }}> rating: {item.rating}*</p>
                <p style={{ color: "black" }}>
                  {" "}
                  description: {item.description}
                </p>
                <GoogleMapComponent
                  latitude={item.latitude}
                  longitude={item.longitude}
                />
                {/* <Orders /> */}
                <DatePicker
                  value={date}
                  onChange={(newDate) => setDate(newDate)}
                  sx={{
                    "& .css-1e6y48t-MuiButtonBase-root-MuiButton-root": {
                      color: "black",
                    },
                  }}
                />
                <StaticTimePicker
                  orientation="landscape"
                  defaultValue={new Date(2000, 1, 1, 0, 0)} // Setăm implicit minutele la 0
                  value={hr}
                  views={["hours"]}
                  minutesStep={null}
                  onChange={(newhr) => setHHr(newhr)}
                  renderInput={() => null}
                  ampmInClock
                  ampm={false}
                  sx={{
                    "& .MuiDialogActions-root": {
                      display: "none",
                    },
                    "& .css-tfwo93-MuiClock-squareMask": {
                      color: "#FF0000",
                    },
                    "& .MuiClock-pin": {
                      backgroundColor: "#FF0000",
                    },
                    "& .MuiClockPointer-root": {
                      backgroundColor: "#FF0000",
                    },
                    "& .MuiClockPointer-thumb": {
                      backgroundColor: "#FF0000",
                      border: "16px solid #FF0000",
                    },
                  }}
                />
                <button onClick={() => handleCreate(item.id)}>Create</button>
              </div>
            );
          })}
        </div>
      </div>
      <Footer />
    </LocalizationProvider>
  );
}

export default Inputs;
