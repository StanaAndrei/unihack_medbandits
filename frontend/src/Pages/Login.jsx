import { useState } from "react";
import { axiosInstanceToApi } from "../../utils/network";
import * as React from "react";
import { useNavigate } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import ImageBackGround from "../assets/logo.png";
import "../App.css";
import Footer from "../../components/Footer";

const defaultTheme = createTheme();

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isCLinic, setIsClinic] = useState(false);

  const navigate = useNavigate();

  const handleLogin = (e) => {
    e.preventDefault();
    console.log(email + " " + password);
    if (isCLinic === true) {
      axiosInstanceToApi
        .post("/clinic/login", {
          email,
          password,
        })
        .then((res) => {
          // Save the token in localStorage
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("isClinic", isCLinic);
          // Navigate to the home page
          navigate("/home");
        })
        .catch((error) => {
          // Handle login error, show error message, etc.
          alert("Login failed", error);
        });
    } else {
      axiosInstanceToApi
        .post("/user/login", {
          email,
          password,
        })
        .then((res) => {
          // Save the token in localStorage
          localStorage.setItem("token", res.data.token);
          // Navigate to the home page
          navigate("/home");
        })
        .catch((error) => {
          // Handle login error, show error message, etc.
          alert("Login failed", error);
        });
    }
  };
  const handleSignUp = () => {
    navigate("/register"); // replace "/signup" with your actual sign-up route
  };
  return (
    <>
      {/* <div>Login</div>
      <input type="text" onChange={(e) => setEmail(e.target.value)} />
      <br />
      <input type="password" onChange={(e) => setPassword(e.target.value)} />
      <br />
      <button onClick={handleLogin}>login</button> */}
      <ThemeProvider theme={defaultTheme}>
        <Grid container component="main" sx={{ height: "100vh" }}>
          <CssBaseline />
          <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
              backgroundImage: `url(${ImageBackGround})`,
              backgroundRepeat: "no-repeat",
              backgroundColor: (t) =>
                t.palette.mode === "light"
                  ? t.palette.grey[50]
                  : t.palette.grey[900],
              backgroundSize: "cover",
              backgroundPosition: "center",
            }}
          />
          <Grid
            item
            xs={12}
            sm={12}
            md={5}
            xl={3}
            component={Paper}
            elevation={6}
            square
            style={{ animation: "fadeInUp 1s" }}
          >
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "#FF0000" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Box
                component="form"
                noValidate
                onSubmit={handleLogin}
                sx={{ mt: 1 }}
              >
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  onChange={(e) => setEmail(e.target.value)}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", // Culoarea subliniaturii când inputul este focusat
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={(e) => setPassword(e.target.value)}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", //
                      backgroundColor: "#FFFFFF",
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      value="remember"
                      style={{ color: "#ff0000" }}
                      onChange={(e) => setIsClinic(e.target.checked)}
                    />
                  }
                  label="Are you a clinic?"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{
                    mt: 3,
                    mb: 2,
                    bgcolor: "#FF0000",
                    "&:hover": {
                      bgcolor: "white",
                      color: "#000000",
                      transition: "2s ease",
                    },
                  }}
                >
                  Sign In
                </Button>
                <Button
                  fullWidth
                  variant="outlined"
                  sx={{
                    mt: 2,
                    mb: 2,
                    borderColor: "#FF0000",
                    color: "#FF0000",
                    "&:hover": {
                      bgcolor: "white",
                      color: "#000000",
                      transition: "2s ease",
                    },
                  }}
                  onClick={handleSignUp}
                >
                  Don't have an account? Sign Up
                </Button>
                <Grid container>
                  <Grid item xs></Grid>
                  <Grid item></Grid>
                </Grid>
                <Footer />
              </Box>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    </>
  );
}

export default Login;
