import React, { useState } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { GoogleMap, LoadScript, Marker, MarkerF } from "@react-google-maps/api";
import Switch from "@mui/material/Switch";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { axiosInstanceToApi } from "../../utils/network";
import "../App.css";
import GoogleMapComponent from "../../components/map";
import Footer from "../../components/Footer";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const defaultTheme = createTheme();

export default function SignUp() {
  const [location, setLocation] = useState({
    lat: 45.7489,
    lng: 21.2087,
  });

  const [manualCoordinates, setManualCoordinates] = useState({
    lat: 0.0,
    lng: 0.0,
  });

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    isClinic: false,
    clinicName: "",
    area: "",
    description: "",
  });
  const [clickCounter, setClickCounter] = useState(0);

  const navigate = useNavigate();
  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const handleMapClick = (event) => {
    const clickCount = clickCounter + 1;
    setClickCounter(clickCount);

    if (clickCount === 3) {
      const newLocation = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng(),
      };
      console.log(newLocation);

      //setLocation(newLocation);
      setManualCoordinates(newLocation);

      // Reset click counter after the third click
      setClickCounter(0);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (formData.isClinic === true) {
      axiosInstanceToApi
        .post("/clinic/", {
          email: formData.email,
          password: formData.password,
          firstName: formData.firstName,
          lastName: formData.lastName,
          area: formData.area,
          name: formData.clinicName,
          latitude: manualCoordinates.lat,
          longitude: manualCoordinates.lng,
          description: formData.description,
        })
        .then((res) => {
          console.log("Clinic created");
          navigate("/login");
        })
        .catch((error) => {
          alert("Failed", error);
        });
      console.log("aaa");
    } else {
      axiosInstanceToApi
        .post("/user/", {
          email: formData.email,
          password: formData.password,
          firstName: formData.firstName,
          lastName: formData.lastName,
        })
        .then((res) => {
          console.log("User created");
          navigate("/login");
        })
        .catch((error) => {
          alert("Failed", error);
        });
    }
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
          style={{ animation: "fadeInUp 1s" }}
        >
          <Avatar sx={{ m: 1, bgcolor: "#FF0000" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  value={formData.firstName}
                  onChange={handleChange}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", //
                      backgroundColor: "#FFFFFF",
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                  value={formData.lastName}
                  onChange={handleChange}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", //
                      backgroundColor: "#FFFFFF",
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={formData.email}
                  onChange={handleChange}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", //
                      backgroundColor: "#FFFFFF",
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  value={formData.password}
                  onChange={handleChange}
                  sx={{
                    "& label.Mui-focused": {
                      color: "#FF0000", // Culoarea etichetei când inputul este focusat
                    },
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#FF0000", //
                      backgroundColor: "#FFFFFF",
                    },
                    "& .MuiOutlinedInput-root": {
                      "& fieldset": {
                        borderColor: "#C4C4C4", // Culoarea chenarului
                      },
                      "&:hover fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului la hover
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                      },
                    },
                  }}
                />
              </Grid>
              {/* <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Switch
                      name="isClinic"
                      checked={formData.isClinic}
                      onChange={handleChange}
                      color="primary"
                    />
                  }
                  label="Are YOU a clinic?"
                />
              </Grid> */}
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="isClinic"
                      checked={formData.isClinic}
                      onChange={handleChange}
                      style={{ color: "#FF0000" }}
                    />
                  }
                  label="ARE YOU A CLINIC?"
                />
              </Grid>
              {formData.isClinic === true ? (
                <>
                  {" "}
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      id="ClinicName"
                      label="Clinic Name"
                      name="clinicName"
                      autoComplete="family-name"
                      value={formData.clinicName}
                      onChange={handleChange}
                      sx={{
                        "& label.Mui-focused": {
                          color: "#FF0000", // Culoarea etichetei când inputul este focusat
                        },
                        "& .MuiInput-underline:after": {
                          borderBottomColor: "#FF0000", //
                          backgroundColor: "#FFFFFF",
                        },
                        "& .MuiOutlinedInput-root": {
                          "& fieldset": {
                            borderColor: "#C4C4C4", // Culoarea chenarului
                          },
                          "&:hover fieldset": {
                            borderColor: "#FF0000", // Culoarea chenarului la hover
                          },
                          "&.Mui-focused fieldset": {
                            borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                          },
                        },
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Select
                      fullWidth
                      label="Area"
                      name="area"
                      value={formData.area}
                      onChange={handleChange}
                      sx={{
                        "& label.Mui-focused": {
                          color: "#FF0000",
                        },
                        "& .MuiInput-underline:after": {
                          borderBottomColor: "#FF0000",
                          backgroundColor: "#FFFFFF",
                        },
                        "& .MuiOutlinedInput-root": {
                          "& fieldset": {
                            borderColor: "#C4C4C4",
                          },
                          "&:hover fieldset": {
                            borderColor: "#FF0000",
                          },
                          "&.Mui-focused fieldset": {
                            borderColor: "#FF0000",
                          },
                        },
                        "& .MuiSelect-icon": {
                          color: "#FF0000", // Culoarea de activ pentru dropdown
                        },
                        "& .MuiSelect-select.MuiSelect-select": {
                          borderColor: "#FF0000", // Culoarea bordurii în dropdown
                        },
                      }}
                    >
                      <MenuItem value="Cardiology">Cardiology</MenuItem>
                      <MenuItem value="Orthopedics">Orthopedics</MenuItem>
                      <MenuItem value="Neurology">Neurology</MenuItem>
                      <MenuItem value="Pediatrics">Pediatrics</MenuItem>
                      <MenuItem value="Dermatology">Dermatology</MenuItem>
                      <MenuItem value="Psychiatry">Psychiatry</MenuItem>
                      <MenuItem value="Endocrinology">Endocrinology</MenuItem>
                      <MenuItem value="Urology">Urology</MenuItem>
                    </Select>
                  </Grid>
                  <LoadScript googleMapsApiKey="AIzaSyBVf0AGGBlJvgOy5_piCpiXMW-4qmL45qQ">
                    <GoogleMap
                      mapContainerStyle={{
                        height: "400px",
                        width: "100%",
                        marginTop: "5%",
                      }}
                      center={location}
                      zoom={12}
                      onClick={handleMapClick}
                    >
                      <MarkerF
                        position={manualCoordinates}
                        key={"marker-unu"}
                      />
                    </GoogleMap>
                  </LoadScript>
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      id="description"
                      label="Description"
                      name="description"
                      autoComplete="description"
                      value={formData.description}
                      onChange={handleChange}
                      sx={{
                        "& label.Mui-focused": {
                          color: "#FF0000", // Culoarea etichetei când inputul este focusat
                        },
                        "& .MuiInput-underline:after": {
                          borderBottomColor: "#FF0000", //
                          backgroundColor: "#FFFFFF",
                        },
                        "& .MuiOutlinedInput-root": {
                          "& fieldset": {
                            borderColor: "#C4C4C4", // Culoarea chenarului
                          },
                          "&:hover fieldset": {
                            borderColor: "#FF0000", // Culoarea chenarului la hover
                          },
                          "&.Mui-focused fieldset": {
                            borderColor: "#FF0000", // Culoarea chenarului când inputul este focusat
                          },
                        },
                      }}
                    />
                  </Grid>
                </>
              ) : null}
            </Grid>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{
                mt: 3,
                mb: 2,
                bgcolor: "#FF0000",
                "&:hover": {
                  bgcolor: "white",
                  color: "#000000",
                  transition: "2s ease",
                },
              }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                {/* <Link href="#" variant="body2">
                  Already have an account? Sign in
                </Link> */}
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Footer />
      </Container>
    </ThemeProvider>
  );
}
