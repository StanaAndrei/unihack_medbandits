import React, { useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import GoogleMapComponent from "../../components/map";
import { axiosInstanceToApi } from "../../utils/network";
import { useNavigate } from "react-router-dom";
import Container from "@mui/material/Container";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Footer from "../../components/Footer";
import Orders from "../../components/Orders";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import "../App.css";

const Home = (props) => {
  const [selectedImage, setSelectedImage] = useState(null);
  const [user, setUser] = useState(null);
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const [appi, setAppi] = useState([null]);
  const [clinicAppi, setClinicAppi] = useState([null]);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const navigate = useNavigate();

  const CustomCard = styled(Card)({
    marginBottom: "20px",

    transition: "box-shadow 0.3s ease-in-out",
    boxShadow:
      "0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12);",
  });

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    setSelectedImage(file);
  };

  const fetchData = async () => {
    try {
      if (localStorage.getItem("isClinic") === "true") {
        const response = await axiosInstanceToApi.get(
          `/clinic/${localStorage.getItem("token")}`
        );
        setUser(response.data);
        const respons_eappi = await axiosInstanceToApi.get(
          `/appointment/getByClinicId/${response.data.id}`
        );
        if (respons_eappi.status === 200) {
          setClinicAppi(respons_eappi.data);
          console.log(respons_eappi.data);
        }
      } else {
        const response = await axiosInstanceToApi.get(
          `/user/${localStorage.getItem("token")}`
        );
        setUser(response.data);

        const responseappi = await axiosInstanceToApi.get(
          `/appointment/${response.data.id}`
        );
        if (responseappi.status === 200) {
          setAppi(responseappi.data);
          console.log(responseappi.data);
        }
      }
    } catch (err) {
      alert("Eroare" + err);
    }
  };

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setDrawerOpen(open);
  };

  const handleDeleteAppi = async (id) => {
    try {
      axiosInstanceToApi
        .delete(`/appointment/${id}`)
        .then((res) => {
          console.log(res);
          fetchData();
        })
        .catch((err) => {});
    } catch (err) {}
  };
  const handleAccept = async (id) => {
    try {
      axiosInstanceToApi
        .patch(`/appointment/accept/${id}`)
        .then((res) => {
          console.log(res);
          fetchData();
        })
        .catch((err) => {});
    } catch (err) {}
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (localStorage.getItem("isClinic") === "true") {
          const response = await axiosInstanceToApi.get(
            `/clinic/${localStorage.getItem("token")}`
          );
          setUser(response.data);
          const respons_eappi = await axiosInstanceToApi.get(
            `/appointment/getByClinicId/${response.data.id}`
          );
          if (respons_eappi.status === 200) {
            setClinicAppi(respons_eappi.data);
            console.log(respons_eappi.data);
          }
        } else {
          const response = await axiosInstanceToApi.get(
            `/user/${localStorage.getItem("token")}`
          );
          setUser(response.data);

          const responseappi = await axiosInstanceToApi.get(
            `/appointment/${response.data.id}`
          );
          if (responseappi.status === 200) {
            setAppi(responseappi.data);
            console.log(responseappi.data);
          }
        }
      } catch (err) {
        alert("Eroare" + err);
      }
    };
    fetchData();
    // getClinicAppi();
  }, []);

  return (
    <div style={{ display: "flex" }}>
      {/* Drawer */}
      <Drawer
        anchor="left"
        open={isDrawerOpen}
        onClose={toggleDrawer(false)}
        width={300}
      >
        <div
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          <List>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Dashboard"
                onClick={() => navigate("/home")}
              />
            </ListItem>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Analyse"
                onClick={() => navigate("/inputs")}
              />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button style={{ width: "300px" }}>
              <ListItemText
                primary="Logout"
                onClick={() => {
                  localStorage.removeItem("token");
                  localStorage.removeItem("isClinic");
                  navigate("/");
                }}
              />
            </ListItem>
          </List>
        </div>
      </Drawer>

      <div style={{ flex: 1 }}>
        {/* AppBar */}
        <AppBar position="static" style={{ backgroundColor: "#FF0000" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer(true)}
              style={{ border: "none", outline: "none" }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" style={{ color: "white" }}>
              MedMe
            </Typography>
          </Toolbar>
        </AppBar>
        <div>
          {user?.isClinic === true ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                //height: "40vh",
                marginTop: "100px",
              }}
            >
              <CustomCard
                style={{
                  width: "80%",
                  maxWidth: "50%",
                  margin: "auto",
                  backgroundColor: "white",
                }}
              >
                <Container
                  maxWidth={isMobile ? "100%" : "lg"} // "lg" pentru 50-70% pe ecrane mari
                  style={{
                    textAlign: "center",
                    marginTop: "20px",
                    paddingBottom: "25px",
                    animation: "fadeInUp 1s",
                  }} // aliniere și spațiu suplimentar de sus
                >
                  <h1 style={{ textAlign: "Center", color: "black" }}>
                    Cont de{" "}
                    <span style={{ fontWeight: "bold", color: "#FF0000" }}>
                      Clinica
                      <br />
                      <br />
                    </span>
                  </h1>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Representative:</b>
                    </span>{" "}
                    {user?.firstName + user.lastName}
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b> Nume:</b>
                    </span>{" "}
                    {user?.name}
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Area:</b>
                    </span>{" "}
                    {user?.area}
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Email:</b>
                    </span>{" "}
                    {user?.email}
                  </Typography>

                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Description:</b>
                    </span>{" "}
                    {user?.description}
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Rating:</b>
                    </span>{" "}
                    {user?.rating}
                  </Typography>
                  <div style={{ width: "100%", marginTop: "20px" }}>
                    <GoogleMapComponent
                      latitude={user?.latitude}
                      longitude={user?.longitude}
                    />
                  </div>
                  <Orders
                    appi={clinicAppi?.filter((e) => e?.isAccepted)}
                    isClinic={user.isClinic}
                  />
                  {clinicAppi.length > 0 && (
                    <div style={{ color: "black" }}>
                      <p>
                        <b>Monthly Appointments</b>
                      </p>
                    </div>
                  )}
                  {clinicAppi?.map((item, index) => {
                    return (
                      <div
                        style={{
                          color: "black",
                          border: "2px #bbb solid",
                          borderRadius: "10px",
                          marginLeft: "auto",
                          marginRight: "auto",
                          marginTop: "20px",
                          paddingBottom: "20px",
                          width: "300px",
                        }}
                        key={index}
                      >
                        <p style={{ color: "black" }}>{item?.startDate}</p>
                        <p style={{ color: "black" }}>{item?.name}</p>
                        <p style={{ color: "#ff0000" }}>{item?.startHour}:00</p>
                        {!item?.isAccepted && (
                          <button onClick={() => handleAccept(item?.id)}>
                            Accept
                          </button>
                        )}
                        <button onClick={() => handleDeleteAppi(item?.id)}>
                          Delete
                        </button>
                      </div>
                    );
                  })}
                </Container>
              </CustomCard>
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                //height: "40vh",
                marginTop: "100px",
              }}
            >
              <CustomCard
                style={{
                  width: "80%",
                  maxWidth: "70%",
                  margin: "auto",
                  height: "100%",
                  backgroundColor: "white",
                  paddingBottom: "2rem",
                }}
              >
                <Container
                  maxWidth={isMobile ? "100%" : "lg"} // "lg" pentru 50-70% pe ecrane mari
                  style={{
                    display: "block !important",
                    textAlign: "center",
                    marginTop: "20px",
                    height: "100%",
                    animation: "fadeInUp 1s",
                  }} // aliniere și spațiu suplimentar de sus
                >
                  <h1 style={{ textAlign: "center", color: "black" }}>
                    Cont de{" "}
                    <span style={{ fontWeight: "bold", color: "#FF0000" }}>
                      <b>Pacient</b>
                    </span>
                  </h1>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>
                        <br />
                        Representative:
                      </b>
                    </span>{" "}
                    {user?.firstName + user?.lastName}
                  </Typography>
                  <Typography
                    variant="h5"
                    color="black"
                    style={{ textAlign: "center", padding: "20px" }}
                  >
                    <span>
                      <b>Email:</b>
                    </span>{" "}
                    {user?.email}
                  </Typography>
                  {appi.length > 0 && (
                    <div
                      style={{
                        color: "black",
                        textAlign: "center",
                        height: "100%",
                      }}
                    >
                      <h1>Appointments</h1>
                      {appi?.map((item, index) => {
                        return (
                          <div
                            style={{
                              color: "black",
                              border: "2px #bbb solid",
                              borderRadius: "10px",
                              marginLeft: "auto",
                              marginRight: "auto",
                              marginTop: "20px",
                              paddingBottom: "20px",
                              width: "300px",
                            }}
                            key={index}
                          >
                            <p>
                              Area:{" "}
                              <span style={{ fontWeight: "bold" }}>
                                {item?.clinicId.area}
                              </span>
                            </p>
                            <p style={{ color: "#FF0000" }}>
                              Clinic: {item?.clinicId.name}
                            </p>
                            <p style={{ color: "black" }}>{item?.startDate}</p>
                            <p style={{ color: "black" }}>{item?.name}</p>
                            <p>{item?.startHour}:00</p>

                            <button onClick={() => handleDeleteAppi(item?.id)}>
                              Delete
                            </button>
                          </div>
                        );
                      })}
                    </div>
                  )}
                </Container>
              </CustomCard>
            </div>
          )}
        </div>
        <Footer /> color: "FF0000"
      </div>
    </div>
  );
};

export default Home;
