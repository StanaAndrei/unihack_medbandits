import { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import PresPage from "./Pages/PresPage";
import Inputs from "./Pages/Inputs";
import Home from "../src/Pages/Home";

function App() {
  const [count, setCount] = useState(0);

  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<PresPage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/home" element={<Home />} />
          <Route path="/Inputs" element={<Inputs />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
