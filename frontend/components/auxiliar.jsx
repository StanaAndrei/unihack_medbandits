import React, { useState } from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import { axiosInstanceToApi } from "../../utils/network";
import "./Inputs.css"; // Assuming styles are moved to a separate CSS file

// Stiluri adaptate de la PresPage
const HoverEffectBox = styled(Box)({
  "&:hover": {
    transform: "scale(1.05)",
    transition: "transform 0.3s ease-in-out",
  },
});

const CustomCard = styled(Card)({
  marginBottom: "20px",
  boxShadow: "0 6px 12px rgba(0, 0, 0, 0.1)",
  transition: "box-shadow 0.3s ease-in-out",
  "&:hover": {
    boxShadow: "0 12px 24px rgba(0, 0, 0, 0.2)",
  },
});

const CustomCardContent = styled(CardContent)({
  backgroundColor: "rgba(255, 255, 255, 0.9)",
  borderRadius: "5px",
});

const StyledContainer = styled(Container)({
  marginTop: "20px",
  padding: "20px",
  backgroundColor: "rgba(255, 255, 255, 0.8)",
  borderRadius: "10px",
  boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
});

// Pagina Inputs
function Inputs() {
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [symptoms, setSymptoms] = useState("");
  
    const handleFileChange = (event) => {
      processFiles(event.target.files);
    };
  
    const handleSymptomsChange = (event) => {
      setSymptoms(event.target.value);
    };
  
    const handleDrop = (event) => {
      event.preventDefault();
      processFiles(event.dataTransfer.files);
      event.dataTransfer.clearData();
    };
  
    const processFiles = (files) => {
      const pdfFiles = Array.from(files).filter(
        (file) => file.type === "application/pdf"
      );
      setSelectedFiles(pdfFiles);
    };
  
    const handleDragOver = (event) => {
      event.preventDefault();
    };
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      const formData = new FormData();
      formData.append("file", selectedFiles[0]);
      // Add symptoms to the formData if needed
      // formData.append('symptoms', symptoms);
  
      try {
        const response = await axiosInstanceToApi.post("/user/upload", formData);
        console.log(response);
      } catch (err) {
        console.error(err);
      }
    };
  
    const handleSymptomsByChatGpt = () => {
      axiosInstanceToApi
        .post("/user/upload-text", {
          description: symptoms,
        })
        .then((res) => {
          console.log(res.data.res);
        })
        .catch((err) => {
          alert("Eroare" + err);
        });
    };
  

  return (
    <StyledContainer>
      <HoverEffectBox>
        <CustomCard>
          <CustomCardContent>
            <Typography variant="h5" style={{ marginBottom: "20px" }}>
              Analyze Your Blood
            </Typography>
            <form className="form">
              <div className="drop-area">
                Drag and drop a PDF file here
              </div>
              <div style={{ display: "flex", width: "auto" }}>
                <input
                  type="file"
                  accept="application/pdf"
                  multiple
                  onChange={handleFileChange}
                  id="fileInput"
                  className="file-input"
                />
                <label htmlFor="fileInput" className="file-label">
                  Choose a file
                </label>
                {selectedFiles.length > 0 && (
                  <div className="file-list">
                    {selectedFiles.map((file, index) => (
                      <div key={index} className="file-name">
                        {file.name}
                      </div>
                    ))}
                  </div>
                )}
                <Button type="submit" variant="contained">
                  Show Results
                </Button>
              </div>
            </form>
          </CustomCardContent>
        </CustomCard>
      </HoverEffectBox>

      <HoverEffectBox>
        <CustomCard>
          <CustomCardContent>
            <Typography variant="h5" style={{ marginBottom: "20px" }}>
              Analyze Your Symptoms
            </Typography>
            <form className="form">
              <textarea
                placeholder="Describe your symptoms"
                value={symptoms}
                onChange={handleSymptomsChange}
                className="textarea"
              />
              <Button
                type="submit"
                variant="contained"
                onClick={handleSymptomsByChatGpt}
              >
                Show Results
              </Button>
            </form>
          </CustomCardContent>
        </CustomCard>
      </HoverEffectBox>
    </StyledContainer>
  );
}

export default Inputs;
