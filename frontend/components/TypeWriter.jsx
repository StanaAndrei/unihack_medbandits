import React, { useState, useEffect } from "react";

const TypewriterText = () => {
  const [text, setText] = useState("");
  const fullText = "MeedMe";
  const speed = 200;

  useEffect(() => {
    let i = 0;

    const startTyping = () => {
      function typeWriter() {
        if (i < fullText.length) {
          setText((prev) => prev + fullText.charAt(i));
          i++;
          setTimeout(typeWriter, speed);
        }
      }

      typeWriter();
    };

    // Start the typewriter effect after a 10-second delay
    const timeoutId = setTimeout(startTyping, 3000); // 10000 milliseconds = 10 seconds

    // Clear the timeout if the component unmounts before the delay is over
    return () => clearTimeout(timeoutId);
  }, []);

  return (
    <div
      id="demo"
      style={{ color: "black", fontSize: "52px", fontWeight: "bold" }}
    >
      {text}
    </div>
  );
};

export default TypewriterText;
