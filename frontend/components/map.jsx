import React, { useEffect } from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";

const containerStyle = {
  width: "100%",
  height: "400px",
};

const initialCenter = {
  lat: 45.75372, // Latitude of Timisoara
  lng: 21.22571, // Longitude of Timisoara
};

const GoogleMapComponent = (props) => {
  const { latitude, longitude } = props;

  return (
    <LoadScript
      googleMapsApiKey="AIzaSyBVf0AGGBlJvgOy5_piCpiXMW-4qmL45qQ"
      libraries={["places"]}
    >
      <GoogleMap
        mapContainerStyle={{
          height: "400px",
          width: "100%",
          marginTop: "5%",
        }}
        center={initialCenter}
        zoom={12}
      >
        {latitude && longitude && (
          <Marker position={{ lat: latitude, lng: longitude }} title="title" />
        )}
      </GoogleMap>
    </LoadScript>
  );
};

export default GoogleMapComponent;
