import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder
from PdfReader import readPdf 
from joblib import dump


data = pd.read_csv('dataset_analize_medicale.csv')



label_encoder = LabelEncoder()
data['Diagnostice'] = label_encoder.fit_transform(data['Diagnostice'])


X = data.drop('Diagnostice', axis=1) 
y = data['Diagnostice']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

param_grid_rf = {
    'n_estimators': [100, 200, 300, 400],
    'max_features': ['sqrt', 'log2'],
    'max_depth': [10, 20, 30, 40],
    'min_samples_split': [2, 5, 10, 15],
    'min_samples_leaf': [1, 2, 4, 6]
}

rf = RandomForestClassifier(random_state=42)
grid_search_rf = GridSearchCV(estimator=rf, param_grid=param_grid_rf, cv=5, n_jobs=-1, verbose=2)
grid_search_rf.fit(X_train, y_train)
best_rf = grid_search_rf.best_estimator_

gb = GradientBoostingClassifier(random_state=42)
gb.fit(X_train, y_train)

rf_pred = best_rf.predict(X_test)
gb_pred = gb.predict(X_test)
print("Random Forest Classification Report:\n", classification_report(y_test, rf_pred))
print("Gradient Boosting Classification Report:\n", classification_report(y_test, gb_pred))


f_cv_score = cross_val_score(best_rf, X, y, cv=5)
print("Random Forest CV Score:", f_cv_score.mean())
gb_cv_score = cross_val_score(gb, X, y, cv=5)
print("Gradient Boosting CV Score:", gb_cv_score.mean())


date_pacient = readPdf("BULETIN ANALIZE") 
df_pacient = pd.DataFrame([date_pacient]) 

predictie_rf = best_rf.predict(df_pacient)
predictie_gb = gb.predict(df_pacient)


predictie_rf_label = label_encoder.inverse_transform(predictie_rf)
predictie_gb_label = label_encoder.inverse_transform(predictie_gb)


print("Predictie Random Forest pentru pacient:", predictie_rf_label[0])
print("Predictie Gradient Boosting pentru pacient:", predictie_gb_label[0])
dump(best_rf, 'model_rf_optimizat.joblib')
dump(gb, 'model_gb.joblib')


