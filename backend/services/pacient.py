import os

import pandas as pd
from joblib import load
from sklearn.preprocessing import LabelEncoder

from services.PdfReader import readPdf


def result(name):
    print(os.getcwd())
    data = pd.read_csv('services//dataset_analize_medicale.csv')
    date_pacient = readPdf(name)

    #date_pacient['Lymfocite'] = 1232
    #date_pacient['Hematocrit'] = 1256

    #diabet
    #date_pacient['Glucoza'] = 350

    #anemie
    #date_pacient['Hematii'] = 3
    #date_pacient['Hemoglobina'] = 10
    #date_pacient['Hematocrit'] = 20

    diagnostice = data['Diagnostice']
    lista_diagnostice = list(set(diagnostice))
    dictionar_nise = dict()
    for chei in lista_diagnostice:
        if chei == 'Infectie Virala':
            dictionar_nise[chei] = 'Endocrinology'
        if chei == 'Anemie':
            dictionar_nise[chei] = 'Cardiology'
        if chei == 'Diabet':
            dictionar_nise[chei] = 'Endocrinology'
        if chei == 'Sanatos':
            dictionar_nise[chei] = '1'
        if chei == 'Leucemie':
            dictionar_nise[chei] = 'Cardiology'
        if chei == 'Trombocitopenie':
            dictionar_nise[chei] = 'Urology'
        if chei == 'Policitemie Vera':
            dictionar_nise[chei] = 'Cardiology'

    df_pacient = pd.DataFrame([date_pacient])

    label_encoder = LabelEncoder()
    label_encoder.fit(diagnostice)

    def load_model(model_path):
        return load(model_path)

    def make_predictions(model, input_data):
        return model.predict(input_data)

    model_rf = load_model('services//model_rf_optimizat.joblib')
    model_gb = load_model('services//model_gb.joblib')

    predictions_rf = make_predictions(model_rf, df_pacient)
    predictions_gb = make_predictions(model_gb, df_pacient)

    predictions_rf_label = label_encoder.inverse_transform(predictions_rf)
    predictions_gb_label = label_encoder.inverse_transform(predictions_gb)

    print("Predicții Random Forest:", predictions_rf_label[0])
    print("Predicții Gradient Boosting:", predictions_gb_label[0])
    if (predictions_rf_label[0] == 'Sanatos'):
        return { 'res': dictionar_nise[predictions_rf_label[0]], 'disease':  predictions_rf_label[0]}
    if predictions_gb_label[0] == 'Sanatos':
        return { 'res': dictionar_nise[predictions_rf_label[0]], 'disease':  predictions_rf_label[0]}
    return {'res': predictions_gb_label[0], 'disease':  predictions_rf_label[0]}
