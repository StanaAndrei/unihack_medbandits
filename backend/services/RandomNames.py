import json
import random
import os

# Function to get random names from one JSON file
def random_names_from_json(file_path, num_names=10):
    try:
        with open(file_path, 'r') as file:
            names = json.load(file)
            return random.sample(names, min(num_names, len(names)))
    except (FileNotFoundError, json.JSONDecodeError, TypeError) as e:
        print(f"Error reading from JSON file: {e}")
        return []

# Function to get balanced random names from two JSON files
def balanced_random_names_from_json(file_path1, file_path2, total_num_names=10):
    try:
        with open(file_path1, 'r') as file1, open(file_path2, 'r') as file2:
            names1 = json.load(file1)
            names2 = json.load(file2)
            num_names_per_file = total_num_names // 2
            extra_name = total_num_names % 2
            sample1 = random.sample(names1, min(num_names_per_file + extra_name, len(names1)))
            sample2 = random.sample(names2, min(num_names_per_file, len(names2)))
            combined_sample = sample1 + sample2
            random.shuffle(combined_sample)
            return combined_sample
    except (FileNotFoundError, json.JSONDecodeError, TypeError) as e:
        print(f"Error reading from JSON file: {e}")
        return []

# Paths to the JSON files
file_path_first_names = os.getcwd() + "/Files/names.json"  # First names file
file_path_last_name1 = os.getcwd() + "/Files/numeF.json"   # Last names file 1
file_path_last_name2 = os.getcwd() + "/Files/numeB.json"   # Last names file 2

# Get the names
first_names = random_names_from_json(file_path_first_names)
last_names = balanced_random_names_from_json(file_path_last_name1, file_path_last_name2)

# Combine first names and last names
full_names = [f"{first} {last}" for first, last in zip(first_names, last_names)]

print(full_names)
