import PyPDF2
import os
import re


def extract_first_string(text):
    if text is None:
        return None

    text = str(text)

    # Caută primul șir de caractere non-spațiu
    first_string_match = re.search(r'\S+', text)
    if first_string_match:
        return first_string_match.group()

    return None


def extract_first_number(text):
    if text is None:
        return None

    text = str(text)

    first_number_match = re.search(r'\b\d+\.\d+|\d+', text)
    if first_number_match:
        try:
            return float(first_number_match.group())
        except ValueError:
            return None
    return None


def readPdf(name):
    path = os.getcwd() + "\\tmp\\" + name
    with open(path, 'rb') as file:
        reader = PyPDF2.PdfReader(file)

        full_text = ""
        for page in reader.pages:
            text = page.extract_text()
            if text:
                full_text += text

    chei = ['Hematii', 'Hemoglobina', 'Hematocrit', 'MCV', 'MCH', 'MCHC', 'Trombocite', 'Neutrofile', 'Lymfocite',
            'Proteine', 'Glucoza']
    dictionar = dict.fromkeys(chei)

    for key in chei:
        if key in full_text:
            start_index = full_text.find(key) + len(key)
            value_start_index = start_index + full_text[start_index:].find(' ') + 1
            value_end_index = full_text[value_start_index:].find('\n') + value_start_index
            dictionar[key] = full_text[value_start_index:value_end_index].strip()

    for key in dictionar.keys():
        if key == 'Proteine' or key == 'Glucoza':
            dictionar[key] = extract_first_string(dictionar[key])
            if dictionar[key] == 'Absent':
                dictionar[key] = 0
            else:
                dictionar[key] = 1
        else:
            dictionar[key] = extract_first_number(dictionar[key])

    return dictionar



