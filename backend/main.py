from flask import Flask, jsonify, make_response
from http import HTTPStatus
from controllers import initControllers
from models import iniDB
from flask_cors import CORS

app = Flask(__name__)

if __name__ == "__main__":
    iniDB()
    CORS(app)
    initControllers(app)
    app.run(port=5000, debug=False)

