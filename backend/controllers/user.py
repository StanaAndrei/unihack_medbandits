import os.path
import time
import jwt
from flask import Blueprint, make_response, request
from http import HTTPStatus
from models.user import User
from werkzeug.utils import secure_filename
from openai import OpenAI
from playhouse.shortcuts import model_to_dict, dict_to_model

from services.PdfReader import readPdf
from services.pacient import result

userBP = Blueprint('user-controller', __name__, url_prefix='/user')


@userBP.route("/", methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    firstName = data.get('firstName')
    lastName = data.get('lastName')
    password = data.get('password')
    try:
        newUserID = User.create(firstName=firstName, lastName=lastName, email=email, password=password)
        return make_response({"id": str(newUserID)}, HTTPStatus.ACCEPTED)
    except Exception as e:
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@userBP.route("/login", methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    try:
        user = User.get(User.email == email, User.password == password)
        eId = jwt.encode({"id": str(user.id)}, "secret", algorithm="HS256")
        return make_response({"token": str(user.id)}, HTTPStatus.OK)
    except Exception as e:
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@userBP.route("/<userId>")
def getUser(userId):
    user = User.get(User.id == userId)
    d = model_to_dict(user)
    d['isClinic'] = False
    return make_response(d, HTTPStatus.OK)


@userBP.route("/upload", methods=['POST'])
def upload():
    if 'file' not in request.files:
        return make_response({}, HTTPStatus.BAD_REQUEST)

    file = request.files['file']
    filename = secure_filename(file.filename)
    file.save(os.path.join('tmp', filename))

    d = result(filename)
    return make_response(d, HTTPStatus.OK)


@userBP.route("/upload-text", methods=['POST'])
def uploadText():
    data = request.get_json()
    description = 'I will give you some symptoms tell me what area of medicine should I visit from this in one single ' \
                  'word:' \
                  'Cardiology Orthopedics Neurology Gastroenterology Pediatrics Ophthalmology Otorhinolaryngology ' \
                  'Psychiatry Dermatology Endocrinology Urology Gynecology Oncology Rheumatology Pneumology ' \
                  'Hematology Nephrology Pathology Gerontology . ' \
                  'The symptoms are: '
    # return make_response({'res': 'Orthopedics'}, HTTPStatus.OK)
    description += data.get('description')
    try:
        api_key = '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'
        client = OpenAI(
            api_key=api_key
        )
        completion = client.completions.create(model='text-davinci-003', prompt=description, max_tokens=1000)
        return make_response({'res': completion.choices[0].text.split(' ')[-1]}, HTTPStatus.OK)
    except Exception as e:
        return make_response({'res': 'Orthopedics'}, HTTPStatus.OK)
