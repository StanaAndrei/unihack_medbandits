import flask.app

from .user import userBP
from .clinic import clinicBP
from .appointment import appointmentBP


def initControllers(app: flask.app.Flask):
    app.register_blueprint(userBP)
    app.register_blueprint(clinicBP)
    app.register_blueprint(appointmentBP)
