from flask import Blueprint, make_response, request
from models.appointment import Appointment
from http import HTTPStatus
from playhouse.shortcuts import model_to_dict, dict_to_model

appointmentBP = Blueprint('appointment-controller', __name__, url_prefix='/appointment')


@appointmentBP.route('/', methods=['POST'])
def create():
    print('----------------------------------------------')
    data = request.get_json()
    startDate = data.get('startDate')
    startHour = data.get('startHour')
    clinicId = data.get('clinicId')
    userId = data.get('userId')
    name = data.get('name')
    index_of_t = startDate.index('T')
    substring_before_t = startDate[:index_of_t]
    try:
        newA = Appointment.create(startDate=substring_before_t, startHour=startHour, clinicId=clinicId, userId=userId, name=name)
        return make_response({}, HTTPStatus.OK)
    except Exception as e:
        print(e)
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@appointmentBP.route('/<userId>')
def getAppointments(userId):
    ans = Appointment.select().where(Appointment.userId == userId)
    res = []
    for instance in ans:
        res.append(model_to_dict(instance))
    return make_response(res, HTTPStatus.OK)


@appointmentBP.route('/<id>', methods=['DELETE'])
def deleteAppointmentById(id):
    Appointment.delete().where(Appointment.id == id).execute()
    return make_response({}, HTTPStatus.OK)


@appointmentBP.route('/accept/<id>', methods=['PATCH'])
def acceptAppointment(id):
    Appointment.update({Appointment.isAccepted: True}).where(Appointment.id == id).execute()
    return make_response({}, HTTPStatus.OK)


@appointmentBP.route('/getIsAcceptedByClinicId/<clinicId>')
def getIsAcceptedByClinicId(clinicID):
    ans = Appointment.select().where(Appointment.clinicId == clinicID & Appointment.isAccepted is True)
    res = []
    for instance in ans:
        res.append(model_to_dict(instance))
    return make_response(res, HTTPStatus.OK)


@appointmentBP.route('/getByClinicId/<clinicId>')
def getClinicId(clinicId):
    ans = Appointment.select().where(Appointment.clinicId == clinicId)
    res = []
    for instance in ans:
        res.append(model_to_dict(instance))
    return make_response(res, HTTPStatus.OK)
