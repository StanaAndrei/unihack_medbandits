import os.path
import time
import jwt
from flask import Blueprint, make_response, request
from http import HTTPStatus
from models.clinic import Clinic
from middlewares.auth import authMW
from services.PdfReader import readPdf
from werkzeug.utils import secure_filename
from playhouse.shortcuts import model_to_dict, dict_to_model

clinicBP = Blueprint('clinic-controller', __name__, url_prefix='/clinic')


@clinicBP.route("/", methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    name = data.get('name')
    area = data.get('area')
    firstName = data.get('firstName')
    lastName = data.get('lastName')
    latitude = data.get('latitude')
    longitude = data.get('longitude')
    description = data.get('description')
    try:
        newClinic = Clinic.create(name=name, area=area, email=email, password=password, firstName=firstName,
                                  lastName=lastName, latitude=latitude, longitude=longitude, description=description)
        return make_response({"id": str(newClinic)}, HTTPStatus.ACCEPTED)
    except Exception as e:
        print(e)
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@clinicBP.route("/login", methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    try:
        clinic = Clinic.get(Clinic.email == email, Clinic.password == password)
        eId = jwt.encode({"id": str(clinic.id)}, "secret", algorithm="HS256")
        return make_response({"token": str(clinic.id)}, HTTPStatus.OK)
    except Exception as e:
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@clinicBP.route("/<userId>")
def getClinic(userId):
    clinic = Clinic.get(Clinic.id == userId)
    d = model_to_dict(clinic)
    d['isClinic'] = True
    return make_response(d, HTTPStatus.OK)


@clinicBP.route('/get-by-area/<area>')
def getClinicByArea(area):
    print('================================================')
    print(area)
    clinics = Clinic.select().where(Clinic.area == area)
    res = []
    for instance in clinics:
        res.append(model_to_dict(instance))
    return make_response(res, HTTPStatus.OK)
