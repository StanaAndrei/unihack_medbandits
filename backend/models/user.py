from .base_model import BaseModel
from peewee import SqliteDatabase, Model, CharField, AutoField, BooleanField


class User(BaseModel):
    id = AutoField(primary_key=True)
    firstName = CharField()
    lastName = CharField()
    email = CharField(unique=True)
    password = CharField()
