from .base_model import db
from .user import User
from .clinic import Clinic
from .appointment import Appointment

def iniDB():
    db.connect()
    db.create_tables([User, Clinic, Appointment])
