from .base_model import BaseModel
from peewee import BooleanField, DateTimeField, CharField, AutoField, ForeignKeyField
from .clinic import Clinic
from .user import User


class Appointment(BaseModel):
    id = AutoField(primary_key=True)
    startDate = CharField()
    startHour = CharField()
    clinicId = ForeignKeyField(Clinic)
    userId = ForeignKeyField(User)
    name = CharField()
    isAccepted = BooleanField(default=False)
