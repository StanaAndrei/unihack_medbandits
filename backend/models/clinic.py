from .base_model import BaseModel
from peewee import SqliteDatabase, Model, CharField, AutoField, DoubleField, TextField


class Clinic(BaseModel):
    id = AutoField(primary_key=True)
    name = CharField()
    area = CharField()
    email = CharField()
    password = CharField()
    firstName = CharField()
    lastName = CharField()
    latitude = DoubleField()
    longitude = DoubleField()
    description = TextField()
    rating = DoubleField(default=3)
